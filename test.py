# Hello this is Palgrave, I'm testing the GitLab CI
import os
if not os.path.exists(".config"):
	open(".config","w").close()
config = open(".config")
if "debug" in config.read():
	debug = True
else:
	debug = False
if debug:
	print("Importing requests....")
try:
	import requests
except Exception:
	print("palgrave: please pip install requests")
if debug:
	print("Importing json....")
import json
import webbrowser

def palgrave(text):
	def respond(response):
		print("< {}".format(response))
	# Processing all commands
	if text == "palgrave":
		respond("Hello!")
	if "goodbye" in text:
		respond("Goodbye. See you later!")
		quit()
	if "what" in text and "weather" in text:
		respond("Where?")
		y = "Athens"
		print("> Athens")
		print("getting weather data...")
		x = requests.post("https://palgrave-weather.kaiete.workers.dev",data=y)
		print("Weather data retrieved... processing")
		print("(weather data {})".format(x.text))
		x = json.loads(x.text)
		respond("The weather right now in {} is {}, and the temperature is {}".format(x["location"]["name"],x["current"]["condition"]["text"],x["current"]["temp_c"]))
		del y
		del x
	if "who" in text or "what" in text and "is" in text or "was" in text:
		text = os.environ["WIKI_ARTICLE"]
		x = requests.get("https://en.wikipedia.org/wiki/" + text.replace(" ","_"))
		x = x.text
		if "Wikipedia does not have an article with that exact name." in x:
			respond("I'm sorry, I don't know who that is.")
		else:
			respond("Here's what I found on Wikipedia")
			webbrowser.open("https://en.wikipedia.org/wiki/" + text.replace(" ","_"))
		
palgrave("palgrave")
palgrave("what weather")
palgrave("who is")

# If you add a command, be sure to add `palgrave("[command]")` above! (also make sure all inputs are automatically filled.)
palgrave("goodbye")
